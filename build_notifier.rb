#!/usr/bin/env ruby

require "nokogiri"
require "net/http"
require "net/https"
require "open-uri"
require "uri"
require "yaml"
require "hashdiff"
require "json"
require 'dotenv'
require 'pp'

# Config
Dotenv.load
kbase = URI('https://support.apple.com/en-us/HT204319')
hipchat_endpoint = "https://api.hipchat.com/v2/room/#{ENV['HIPCHAT_ROOM']}/notification?auth_token=#{ENV['HIPCHAT_TOKEN']}"

class String
  def convert_nbsp_to_ascii
    return self.gsub(/[[:space:]]/, ' ')
  end
end

# Fetch
html = open(kbase)

# Parse with Nokogiri
doc = Nokogiri::HTML(html) do |config|
  config.noblanks
end

# Hash to store our new builds
builds = Hash.new

# Loop through the HTML table
doc.css('section table tr').each do |q|
  next if q["id"] == "header"
  row = q.css("td")
  
  mac = row[0].content.convert_nbsp_to_ascii.strip
  build_numbers = row[4].content.strip.split(",").map {|build| "#{build.convert_nbsp_to_ascii.strip}" }
  
  build_numbers.each do |build| 
    builds[build] ||= Array.new
    builds[build] << mac
  end

end

# Load our old builds
old_builds = YAML.load(open("builds.yaml"))

# Compute difference
diff = HashDiff.diff(old_builds, builds)
if (diff.size > 0)
  color = :yellow
  text_diff = "New OS X factory versions detected:\n"
  text_diff += diff.map {|line| line.join(" ")}.join("\n")
else
  color = :green
  text_diff = "No new OS X factory versions detected"
end

# Notify

# Configure HTTP POST
uri = URI.parse(hipchat_endpoint)
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

# Build request
request = Net::HTTP::Post.new(uri.request_uri)
request.content_type = "application/json"
request.body = {
  color: color, 
  message_format: :text,
  message: text_diff
}.to_json

# Send to HipChat
# puts request.body
response = http.request(request)

# Save
File.open("builds.yaml", "w") do |file|
  file.puts builds.to_yaml
end
